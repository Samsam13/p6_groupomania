import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/network',
    name: 'HomeView',
    component: () => import(/*webpackChunkName : "register"*/ '../views/HomeView.vue')
  },
  {
    path: '/register',
    name: 'RegisterView',
    component: () => import(/*webpackChunkName : "register"*/ '../views/RegisterView.vue')
  }

]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
